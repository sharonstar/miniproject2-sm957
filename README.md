# miniProject2-sm957



## My function
Giving a radius, the funtion will return the area.

## Test my function
Run the Lambda emulator built in with the watch subcommand:
``` bash
cargo lambda watch
``` 

The basic function receives events with a radius field in the payload, you can invoke them with the following command:
``` bash
cargo lambda invoke --data-ascii "{ \"radius\": \"3\" }"
``` 
